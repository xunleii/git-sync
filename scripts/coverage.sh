# Based on https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/
#!/usr/bin/env sh

PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in ${PKG_LIST}; do
    go test -covermode=count -coverprofile "${package##*/}.cov" "$package" ;
done

tail -q -n +2 *.cov >> global.cov
if [ "$1" = "html" ]
then
	go tool cover -html=global.cov -o coverage.html
else
	go tool cover -func=global.cov
fi
rm -rf *.cov