package gitlab

import (
	"github.com/spf13/pflag"

	"gitlab.com/xunleii/git-sync/provider"
)

var token string
var gitlabProvider = provider.Provider{
	Name:  "gitlab",
	Flags: &pflag.FlagSet{},
	Sync:  gitlabSync,
}

func init() {
	gitlabProvider.Flags.StringVarP(&token, "token", "t", "", "Gitlab access token (must have API rights)")

	provider.RegisterProvider(&gitlabProvider)
}
