package gitlab

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/xunleii/git-sync/provider"
)

func defaultClient(opts provider.SyncOptions) (*http.Client, error) {
	rootCAs, _ := x509.SystemCertPool()
	if rootCAs == nil {
		rootCAs = x509.NewCertPool()
	}

	if opts.Certs != "" {
		logrus.
			WithField("provider", "gitlab").
			Debugf("gitlab: add certificates '%s'", opts.Certs)

		certs, err := ioutil.ReadFile(opts.Certs)
		if err != nil {
			return nil, fmt.Errorf("failed to read custom certificates: %w", err)
		}

		if !rootCAs.AppendCertsFromPEM(certs) {
			logrus.Warnf("gitlab: no certificates appended, using system certificates only")
		}
	}

	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: opts.Insecure,
				RootCAs:            rootCAs,
			},
		},
		Timeout: 5 * time.Second,
	}, nil
}

