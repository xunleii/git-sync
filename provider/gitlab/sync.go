package gitlab

import (
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/xunleii/git-sync/provider"
)

func gitlabSync(projectURL string, opts provider.SyncOptions) ([]provider.Project, error) {
	if token == "" {
		token, _ = os.LookupEnv("GITLAB_ACCESS_TOKEN")
	}

	if !strings.Contains(projectURL, "://") {
		projectURL = "http://" + projectURL
	}

	url, err := url.ParseRequestURI(projectURL)
	if err != nil {
		return nil, fmt.Errorf("invalid Gitlab URL: %w", err)
	}

	logrus.
		WithField("provider", "gitlab").
		Debugf("gitlab: create new client for '%s'", fmt.Sprintf("%s://%s", url.Scheme, url.Host))

	client, err := defaultClient(opts)
	if err != nil {
		return nil, err
	}

	git, err := gitlab.NewClient(
		token,
		gitlab.WithHTTPClient(client),
		gitlab.WithBaseURL(fmt.Sprintf("%s://%s", url.Scheme, url.Host)),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create a Gitlab client: %w", err)
	}

	fullPath := strings.Trim(url.Path, "/")

	projects, err := projectsFromPath(git, fullPath)
	if err != nil {
		return nil, err
	}

	// Workaround for gitlab issue #61722 (https://gitlab.com/gitlab-org/gitlab-ce/issues/61722)
	// Use a blank token to clone public API
	if token != "" && len(projects) == 0 {
		logrus.
			WithField("provider", "gitlab").
			WithField("workaround", "https://gitlab.com/gitlab-org/gitlab-ce/issues/61722").
			Debugf("gitlab: create new client for '%s'", fmt.Sprintf("%s://%s", url.Scheme, url.Host))

		git, err = gitlab.NewClient(
			"",
			gitlab.WithHTTPClient(client),
			gitlab.WithBaseURL(fmt.Sprintf("%s://%s", url.Scheme, url.Host)),
		)
		if err != nil {
			return nil, fmt.Errorf("failed to create a Gitlab client: %w", err)
		}

		projects, err = projectsFromPath(git, fullPath)
		if err != nil {
			return nil, err
		}
	}

	if len(projects) == 0 {
		// Single repository or not exists
		return nil, fmt.Errorf("group or user '%s' not found (use `git clone` for a single repository)", fullPath)
	}
	return projects, nil
}

func projectsFromPath(git *gitlab.Client, path string) ([]provider.Project, error) {
	parts := strings.Split(path, "/")
	target := parts[len(parts)-1]

	// Check with groups
	groups, _, err := git.Groups.SearchGroup(target)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch groups '%s': %w", target, err)
	}
	group := funk.Find(groups, func(group *gitlab.Group) bool { return group.FullPath == path })
	if group != nil {
		return projectsFromGroup(git, group.(*gitlab.Group))
	}

	// Check with users
	users, _, err := git.Users.ListUsers(&gitlab.ListUsersOptions{Search: &target})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch users '%s': %w", target, err)
	}
	user := funk.Find(users, func(user *gitlab.User) bool { return user.Username == target })
	if user != nil {
		return projectsFromUser(git, user.(*gitlab.User))
	}

	return nil, nil
}

func projectsFromGroup(git *gitlab.Client, group *gitlab.Group) ([]provider.Project, error) {
	logrus.
		WithField("provider", "gitlab").
		Debugf("gitlab: get project list of '%s'", group.FullPath)

	gprojects, _, err := git.Groups.ListGroupProjects(group.ID, &gitlab.ListGroupProjectsOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch project list of '%s': %w", group.FullPath, err)
	}

	var projects []provider.Project
	for _, project := range gprojects {
		projects = append(projects, provider.Project{
			Path: project.PathWithNamespace,
			HTTP: project.HTTPURLToRepo,
			SSH:  project.SSHURLToRepo,
		})
	}

	logrus.
		WithField("provider", "gitlab").
		Debugf("gitlab: get subgroup list of '%s'", group.FullPath)

	subgroups, _, err := git.Groups.ListSubgroups(group.ID, &gitlab.ListSubgroupsOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch subgroup list of '%s': %w", group.FullPath, err)
	}

	for _, group := range subgroups {
		subprojects, err := projectsFromGroup(git, group)
		if err != nil {
			return nil, err
		}

		projects = append(projects, subprojects...)
	}
	return projects, nil
}

func projectsFromUser(git *gitlab.Client, user *gitlab.User) ([]provider.Project, error) {
	logrus.
		WithField("provider", "gitlab").
		Debugf("gitlab: get project list of '%s'", user.Name)

	gprojects, _, err := git.Projects.ListUserProjects(user.ID, &gitlab.ListProjectsOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch project list of '%s': %w", user.Name, err)
	}

	var projects []provider.Project
	for _, project := range gprojects {
		projects = append(projects, provider.Project{
			Path: project.PathWithNamespace,
			HTTP: project.HTTPURLToRepo,
			SSH:  project.SSHURLToRepo,
		})
	}

	//TODO: List all groups
	return projects, nil
}
