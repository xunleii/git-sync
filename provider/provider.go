package provider

import (
	"fmt"

	"github.com/spf13/pflag"
	"golang.org/x/xerrors"
)

var providers = map[string]*Provider{}

// Project represents a git repository. It contains path
// where git must clone the project, and repository urls.
type Project struct {
	Path string
	SSH  string
	HTTP string
}

// Provider represents a git provider which can be used to
// clone many repositories at once (called 'synchronisation').
type Provider struct {
	Name  string
	Flags *pflag.FlagSet
	Sync  func(url string, opts SyncOptions) ([]Project, error)
}

// SyncOptions contains options used by the provider Sync
// methods.
type SyncOptions struct {
	Insecure bool
	Certs    string
}

// RegisterProvider registers a new provider in the registry.
func RegisterProvider(provider *Provider) {
	if _, exists := providers[provider.Name]; exists {
		panic(fmt.Sprintf("provider '%s' already registred", provider.Name))
	}
	providers[provider.Name] = provider
}

// RegisteredProvider returns a provider if registered
func RegisteredProvider(name string) (*Provider, error) {
	provider, exists := providers[name]

	if !exists {
		return nil, xerrors.Errorf("provider '%s' not registered", name)
	}
	return provider, nil
}

// Registry returns all providers.
func Registry() map[string]*Provider {
	return providers
}
