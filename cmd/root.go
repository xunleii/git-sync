package cmd

import (
	"fmt"
	"math"
	"os"
	"runtime"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/xunleii/git-sync/pkg/gitsync"
	"gitlab.com/xunleii/git-sync/provider"
)

var gsync = gitsync.GitSync{
	Config: gitsync.Config{
		ParallelCloningThreshold: int(math.Ceil(float64(runtime.NumCPU()) / 2)),
	},
}

// git-sync main definition
var rootCmd = &cobra.Command{
	Use:              "sync",
	Short:            "Git sync clone multiple repository at once using provider API",
	PersistentPreRun: func(cmd *cobra.Command, args []string) { setupLogrus(cmd) },
}

// Initializes git-sync command and setup logrus
func init() {
	setupCobra(rootCmd)
	rootCmd.PersistentFlags().BoolVarP(&gsync.Verbose, "verbose", "v", false, "Make the operation more talkative")
	rootCmd.PersistentFlags().BoolVarP(&gsync.DryRun, "dry-run", "n", false, "If true, only print the objects that would be clone, without clone them")
	rootCmd.PersistentFlags().BoolVarP(&gsync.UseSSH, "ssh", "S", true, "Clone over SSH instead of HTTP(S)")
	rootCmd.PersistentFlags().BoolVar(&gsync.EnableSubmodule, "--recurse-submodules", true, "Initialize submodules in the clone")

	rootCmd.PersistentFlags().BoolVarP(&gsync.SyncOptions.Insecure, "insecure", "k", false, "If true, TLS accepts any certificate presented by the server and any host name in that certificate")
	rootCmd.PersistentFlags().StringVar(&gsync.SyncOptions.Certs, "certs", "", "Use custom certificate to talk with the API")
	_ = rootCmd.MarkFlagFilename("certs")

	logrus.SetLevel(logrus.DebugLevel)
}

// Execute setups provider commands and starts git-sync command
func Execute() {
	for _, provider := range provider.Registry() {
		providerCmd := &cobra.Command{
			Use:   fmt.Sprintf("%s <repository> [<directory>]", provider.Name),
			Short: fmt.Sprintf("Synchronize with '%s' provider", provider.Name),

			Args: validateArguments,
			RunE: func(_ *cobra.Command, args []string) error { return gsync.Synchronize(provider, args) },

			DisableFlagsInUseLine: true,
		}

		providerCmd.PersistentFlags().AddFlagSet(provider.Flags)
		rootCmd.AddCommand(providerCmd)
	}

	if err := rootCmd.Execute(); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s", err)
		os.Exit(1)
	}
}

// validateArguments validates arguments given to a provider
func validateArguments(cmd *cobra.Command, args []string) error {
	switch {
	case len(args) == 0 || len(args) > 2:
		_ = cmd.Usage()
		os.Exit(1)
	case len(args) == 2 && !dirExists(args[1]):
		logrus.Fatalf("%s: directory %s does not exists", cmd.Name(), args[1])
	}
	return nil
}

// dirExists returns true if the argument is a directory, else false
func dirExists(directory string) bool {
	stat, err := os.Stat(directory)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return stat.IsDir()
}
