package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"golang.org/x/crypto/ssh/terminal"
)

func setupLogrus(_ *cobra.Command) {
	logrus.SetOutput(os.Stderr)
	logrus.SetLevel(logrus.InfoLevel)
	if gsync.Verbose {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		logrus.SetFormatter(&prefixed.TextFormatter{
			DisableTimestamp: !gsync.Verbose,
			ForceColors:      true,
		})
	} else {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
}
