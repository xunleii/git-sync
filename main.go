package main

import (
	_ "gitlab.com/xunleii/git-sync/provider/gitlab"

	"gitlab.com/xunleii/git-sync/cmd"
)

func main() {
	cmd.Execute()
}
