module gitlab.com/xunleii/git-sync

go 1.14

require (
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/thoas/go-funk v0.6.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	github.com/xanzy/go-gitlab v0.31.0
	golang.org/x/crypto v0.0.0-20200427165652-729f1e841bcc
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
	gopkg.in/src-d/go-git.v4 v4.13.1
)
