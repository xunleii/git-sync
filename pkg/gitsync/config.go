package gitsync

import (
	"gopkg.in/src-d/go-git.v4"

	"gitlab.com/xunleii/git-sync/provider"
)

const defaultSubmoduleRescursivity = 8

// Config represents all configuration resources needed by git-sync
type Config struct {
	Verbose                  bool
	EnableSubmodule          bool
	DryRun                   bool
	UseSSH                   bool
	ParallelCloningThreshold int

	SyncOptions provider.SyncOptions
}

// SubmoduleRecursionDepth returns the depth of submodule clone recursion
func (cfg Config) SubmoduleRecursionDepth() git.SubmoduleRescursivity {
	if cfg.EnableSubmodule {
		return git.SubmoduleRescursivity(defaultSubmoduleRescursivity)
	}
	return git.SubmoduleRescursivity(0)
}