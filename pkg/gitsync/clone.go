package gitsync

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/src-d/go-git.v4"

	"gitlab.com/xunleii/git-sync/provider"
)

func (gsync GitSync) clone(project provider.Project, target string) {
	defer gsync.Done()
	defer func() { <-gsync.cloneThreshold }()
	gsync.cloneThreshold <- struct{}{}

	url := project.SSH
	if !gsync.UseSSH {
		url = project.HTTP
	}

	if gsync.DryRun {
		logrus.Infof("%s: dry-run enabled, skip cloning into %s", project.Path, target)
		return
	}

	logrus.Infof("cloning %s into %s", project.Path, target)
	_, err := git.PlainClone(target, false, &git.CloneOptions{
		URL:               url,
		RecurseSubmodules: gsync.SubmoduleRecursionDepth(),
	})

	if err != nil {
		logrus.Warnf("%s: %v", project.Path, err)
		return
	}
}
