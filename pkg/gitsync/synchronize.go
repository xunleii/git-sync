package gitsync

import (
	"fmt"
	"os"
	"path"
	"sync"

	"github.com/sirupsen/logrus"

	"gitlab.com/xunleii/git-sync/provider"
)

// GitSync represents an instance of git-sync tool
type GitSync struct {
	Config

	*sync.WaitGroup
	cloneThreshold chan interface{}
}

// Synchronize use a provider to get a list of repository to be cloned and clone them
func (gsync GitSync) Synchronize(provider *provider.Provider, args []string) error {
	assertProvider(provider)

	target, _ := os.Getwd()
	if len(args) > 1 && args[1] != "." {
		target = args[1]
	}

	logrus.Infof("%s: get list of repository from %s", provider.Name, args[0])
	projects, err := provider.Sync(args[0], gsync.SyncOptions)
	switch {
	case err != nil && gsync.Verbose:
		logrus.Fatalf("%s: %+v", provider.Name, err)
	case err != nil:
		logrus.Fatalf("%s: %v", provider.Name, err)
	case len(projects) == 0:
		logrus.Fatalf("%s: not repository found for %s", provider.Name, target)
	default:
		logrus.WithField("provider", provider.Name).Debugf("%s: %d repositories found", provider.Name, len(projects))
	}

	logrus.WithField("provider", provider.Name).Debugf("%s: threshold cloning set to %d goroutines", provider.Name, gsync.ParallelCloningThreshold)
	gsync.cloneThreshold = make(chan interface{}, gsync.ParallelCloningThreshold)

	gsync.WaitGroup = &sync.WaitGroup{}
	for i := range projects {
		project := projects[i]
		target := path.Join(target, project.Path)

		gsync.Add(1)
		go gsync.clone(project, target)
	}
	gsync.Wait()
	return nil
}

func assertProvider(p *provider.Provider) {
	if p.Sync == nil {
		panic(fmt.Sprintf("invalid provider %s: provider.Sync cannot be null", p.Name))
	}
}
